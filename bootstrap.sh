#!/bin/sh

mkdir server & cd server

apk add wget curl libc6-compat rcon libstdc++

tee -a ~/.rconrc <<EOF
  [minecraft]
  hostname = 127.0.0.1
  port = 25575
  password = 123
  minecraft = true
EOF

curl -s https://api.github.com/repos/nothub/mrpack-install/releases/latest \
| grep "mrpack-install-linux" \
| cut -d : -f 2,3 \
| tr -d \" \
| wget -qi -

curl -s https://api.github.com/repos/Arno500/mrpack-install/releases/latest \
| grep "mrpack-install-linux" \
| cut -d : -f 2,3 \
| tr -d \" \
| wget -O mrpack-install-linux-client -qi - 

chmod +x mrpack-install-linux
chmod +x mrpack-install-linux-client

# Acktually MRPack doesn't remove old mods
rm -rf mc/mods
rm -rf mc/automodpack/host-modpack/config
rm -rf mc/automodpack/host-modpack/mods

./mrpack-install-linux --server-file fabric-server.jar "https://gitlab.com/Arno500/minecraft-fabric-server/-/raw/${CI_COMMIT_SHA}/server%201.19.2.mrpack?inline=false"
./mrpack-install-linux-client --server-file fabric-server.jar --server-dir ./mc/automodpack/host-modpack "https://gitlab.com/Arno500/minecraft-fabric-server/-/raw/${CI_COMMIT_SHA}/client%201.19.2.mrpack?inline=false"

rm -rf mc/automodpack/host-modpack/fabric-server.jar

cd mc

exec java -Xms1000m -Xmx3000m -XX:+UseG1GC -XX:+ParallelRefProcEnabled -XX:MaxGCPauseMillis=200 -XX:+UnlockExperimentalVMOptions -XX:+AlwaysPreTouch -XX:G1NewSizePercent=30 -XX:G1MaxNewSizePercent=40 -XX:G1HeapRegionSize=8M -XX:G1ReservePercent=20 -XX:G1HeapWastePercent=5 -XX:G1MixedGCCountTarget=4 -XX:InitiatingHeapOccupancyPercent=15 -XX:G1MixedGCLiveThresholdPercent=90 -XX:G1RSetUpdatingPauseTimePercent=5 -XX:SurvivorRatio=32 -XX:+PerfDisableSharedMem -XX:MaxTenuringThreshold=1 -jar fabric-server.jar nogui
