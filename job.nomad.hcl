job "minecraft-fabric" {
  type = "service"

  group "app" {
    task "minecraft" {
      driver = "podman"

      kill_timeout = "2m"

      config {
        image = "docker://eclipse-temurin:20-jdk-alpine"
        ports = ["minecraft", "automodpack"]
        volumes = [
          "/mnt/b/volumes/minecraft:/app/server/mc:Z,noexec"
        ]
        entrypoint = ["/bin/sh", "-c", "cp ${NOMAD_TASK_DIR}/bootstrap.sh / && chmod +x /bootstrap.sh && /bootstrap.sh"]
        working_dir = "/app"
        memory_reservation = "500m"
        memory_swap = "4000m"
        cpu_hard_limit = true
      }

      resources {
        cpu = 6288
        memory_max = 3000
      }

      env {
        CI_COMMIT_SHA = "${CI_COMMIT_SHA}"
      }

      artifact {
        source       = "https://gitlab.com/Arno500/minecraft-fabric-server/-/raw/${CI_COMMIT_SHA}/bootstrap.sh?inline=false"        
      }

      service {
        name = "minecraft"
        port = "minecraft"
        provider = "consul"
      }

      service {
        name = "bluemap"
        port = "bluemap"
        provider = "consul"
        tags = [
          "traefik.http.routers.mc-map.rule=Host(`mc-map.arnodubo.is`)",
          "traefik.http.routers.mc-map.entrypoints=websecure",
          "traefik.http.routers.mc-map.tls=true",
          "traefik.enable=true",
        ]
      }
    }

    network {
      mode = "bridge"
      port "minecraft" { static = 25565 }
      port "bluemap" { to = 8100 }
      port "automodpack" { static = 30037 }
    }

    update {
        max_parallel = 0
    }
  }

  reschedule {
      interval       = "3m"
      delay          = "30s"
      delay_function = "constant"
      unlimited      = true
    }
}
